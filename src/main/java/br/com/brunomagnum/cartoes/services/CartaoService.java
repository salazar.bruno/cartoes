package br.com.brunomagnum.cartoes.services;

import br.com.brunomagnum.cartoes.models.Cartao;
import br.com.brunomagnum.cartoes.models.Cliente;
import br.com.brunomagnum.cartoes.models.DTOs.CartaoEntradaDTO;
import br.com.brunomagnum.cartoes.models.DTOs.CartaoSaidaDTO;
import br.com.brunomagnum.cartoes.repositories.CartaoRepository;
import br.com.brunomagnum.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private ClienteService clienteService;

    public CartaoSaidaDTO criaCartao(CartaoEntradaDTO cartaoEntradaDTO){

        //Optional<Cliente> clienteOptional = clienteRepository.findById(clienteId);
        Cliente cliente = clienteService.buscaClientePorId(cartaoEntradaDTO.getClienteId());

        if (cliente != null) {

            Cartao cartao = new Cartao();
            cartao.setNumero(cartaoEntradaDTO.getNumero());
            cartao.setClienteId(cliente);
            cartao.setAtivo(false);
            cartaoRepository.save(cartao);

            CartaoSaidaDTO cartaoSaidaDTO = new CartaoSaidaDTO();
            cartaoSaidaDTO.setNumero(cartao.getNumero());
            cartaoSaidaDTO.setAtivo(cartao.isAtivo());
            cartaoSaidaDTO.setClienteId(cartao.getClienteId().getId());
            cartaoSaidaDTO.setId(cartao.getId());
            return cartaoSaidaDTO;
        }
        else {
            throw new RuntimeException("Cliente não ecziste");
        }

    }

    public Cartao buscaCartaoPorId(Integer id){
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);

        if (cartaoOptional.isPresent()){
            Cartao cartao = new Cartao();
            cartao.setId(cartaoOptional.get().getId());
            cartao.setNumero(cartaoOptional.get().getNumero());
            cartao.setClienteId(cartaoOptional.get().getClienteId());
            cartao.setAtivo(cartaoOptional.get().isAtivo());

            return cartao;
        } else{
            throw new RuntimeException("ID de Cartao nao encontrado");
        }
    }

    public CartaoSaidaDTO mudaStatusCartao(String numero, boolean ativo) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);

        if (cartaoOptional.isPresent()){
            Cartao cartao = new Cartao();
            cartao.setAtivo(ativo);
            cartaoRepository.save(cartao);

            CartaoSaidaDTO cartaoSaidaDTO = new CartaoSaidaDTO();
            cartaoSaidaDTO.setId(cartao.getId());
            cartaoSaidaDTO.setClienteId(cartao.getClienteId().getId());
            cartaoSaidaDTO.setNumero(cartao.getNumero());
            cartaoSaidaDTO.setAtivo(cartao.isAtivo());

            return cartaoSaidaDTO;
        } else{
            throw new RuntimeException("ID de Cartao nao encontrado");
        }
    }

}
