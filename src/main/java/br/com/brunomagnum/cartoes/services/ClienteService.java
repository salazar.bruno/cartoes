package br.com.brunomagnum.cartoes.services;

import br.com.brunomagnum.cartoes.models.Cliente;
import br.com.brunomagnum.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criaCliente(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Cliente buscaClientePorId(Integer id){
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if (clienteOptional.isPresent()){
            Cliente cliente = new Cliente();
            cliente.setId(clienteOptional.get().getId());
            cliente.setName(clienteOptional.get().getName());
            return cliente;
        }
        else{
            throw new RuntimeException("Cliente não encontrado");
        }
    }
}
