package br.com.brunomagnum.cartoes.controllers;

import br.com.brunomagnum.cartoes.models.Cartao;
import br.com.brunomagnum.cartoes.models.Cliente;
import br.com.brunomagnum.cartoes.models.DTOs.CartaoEntradaDTO;
import br.com.brunomagnum.cartoes.models.DTOs.CartaoSaidaDTO;
import br.com.brunomagnum.cartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @GetMapping("/{id}")
    public Cartao buscaCartaoPorId(@PathVariable(name="id") Integer id){
        try{
            return cartaoService.buscaCartaoPorId(id);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PostMapping
    public ResponseEntity<CartaoSaidaDTO> criaCartao(@RequestBody @Valid CartaoEntradaDTO cartaoEntradaDTO){
        try{
            return ResponseEntity.status(201).body(cartaoService.criaCartao(cartaoEntradaDTO));
        } catch(RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,exception.getMessage());
        }
    }

    @PatchMapping("/{numero}")
    public ResponseEntity<CartaoSaidaDTO> mudaStatusCartao(@PathVariable(name="numero") String numero, @RequestBody Boolean ativo){
        try{
            return ResponseEntity.status(200).body(cartaoService.mudaStatusCartao(numero, ativo));
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }


}
