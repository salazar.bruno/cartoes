package br.com.brunomagnum.cartoes.repositories;

import br.com.brunomagnum.cartoes.models.Cartao;
import jdk.nashorn.internal.runtime.options.Option;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {
    Optional<Cartao> findByNumero(String numero);
}
