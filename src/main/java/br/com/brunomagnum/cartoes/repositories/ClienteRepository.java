package br.com.brunomagnum.cartoes.repositories;

import br.com.brunomagnum.cartoes.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository  extends CrudRepository<Cliente, Integer> {
}
